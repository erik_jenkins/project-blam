/**
 * Created by Jim on 1/20/15.
 */

package bodies;


import math.*;
import static org.lwjgl.opengl.GL11.*;

public abstract class Actor extends GameObject {

    private Vector position, velocity, acceleration, boundBox;

    public Actor() {}

    public Actor(Vector bb) {}

    public Actor(Vector pos, Vector bb) {}

    public Actor(Vector pos, Vector vel, Vector bb) {}

    public Actor(Vector pos, Vector vel, Vector acc, Vector bb) {}



    public void setVelocity(Vector vel){
        velocity = vel;
    }

    public void setAcceleration(Vector acc){
        acceleration = acc;
    }

    public Vector velocity(){
        return velocity;
    }

    public Vector acceleration(){
        return acceleration;
    }

}
