/**
 * Created by jim on 1/25/15.
 */

package bodies;

import math.Vector;


public abstract class GameObject {

    private Vector position, velocity, boundBox;
    private double mass;
    public static final double INFINITY = 999999;
    public enum ObjectType {PLAYER, WALL};
    public ObjectType type;

    public GameObject() {}


    public void setPosition(Vector pos){
        position = pos;
    }

    public void setVelocity(Vector vel) { velocity = vel; }

    public void setBoundBox(Vector bb) { boundBox = bb; }

    public Vector position(){
        return position;
    }

    public Vector velocity() { return velocity; }

    public Vector getBoundBox() { return boundBox; }

    public double getMass() { return mass; }

    public void update() {}

    public void draw() {}

}
