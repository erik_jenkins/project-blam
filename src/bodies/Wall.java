/**
 * Created by jim on 1/25/15.
 */

package bodies;


import math.Vector;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL11.glEnd;
import static org.lwjgl.opengl.GL11.glVertex2d;

public class Wall extends GameObject{

    private Vector position, velocity, boundBox;
    public ObjectType type;
    private double mass = INFINITY;


    // used for type checking -> there has to be a better way to do this...
    public Wall() {};

    public Wall(Vector pos, Vector bb) {
        position = pos;
        velocity = new Vector(0.0, 0.0);
        boundBox = bb;
        type = ObjectType.WALL;
    }


    public void setPosition(Vector pos){
        position = pos;
    }

    public void setBoundBox(Vector bb) { boundBox = bb; }

    public Vector position(){
        return position;
    }

    public Vector velocity() { return velocity; }

    public Vector getBoundBox() { return boundBox; }

    public void draw() {

        glBegin(GL_QUADS);

        glColor3d(0.6, 0.2, 0.1);

        glVertex2d(position().x, position().y);
        glVertex2d(position().x + boundBox.x, position().y);

        glVertex2d(position().x + boundBox.x, position().y + boundBox.y);
        glVertex2d(position().x, position().y + boundBox.y);

        glEnd();
    }
}
