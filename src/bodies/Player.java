/**
* Created by Jim on 1/20/15.
*/

package bodies;

import math.*;
import static org.lwjgl.opengl.GL11.*;
import org.lwjgl.input.Keyboard;


public class Player extends Actor {

    private Vector position, velocity, acceleration, boundBox;
    public Vector gravity;
    private double mass;
    public boolean isGrounded;
    private int framesOnGround;
    public ObjectType type;


    private boolean jumpPressed = false, firstFrameOnGround, jumpWasPressed = false;
    private float jumpsRemaining = 2, jumpCounter, jumpVelocity;



    // used for type checking -> there has to be a better way to do this...
    public Player() {}


    public Player(Vector pos, Vector bb) {
        position = pos;
        velocity = new Vector(0, 0);
        acceleration = new Vector(0, 0);
        gravity = new Vector(0, -.34);
        boundBox = bb;
        mass = 10;

        isGrounded = false;
        firstFrameOnGround = false;
        framesOnGround = 0;

        jumpCounter = 2;
        jumpVelocity = 7;

        type = ObjectType.PLAYER;
    }


    public void setPosition(Vector pos){
        position = pos;
    }

    public void setBoundBox(Vector bb) { boundBox = bb; }

    public Vector position(){
        return position;
    }

    public Vector getBoundBox() { return boundBox; }

    public void setVelocity(Vector vel){
        velocity = vel;
    }

    public void setAcceleration(Vector acc){
        acceleration = acc;
    }

    public Vector velocity(){
        return velocity;
    }

    public Vector acceleration(){
        return acceleration;
    }

    public double getMass() { return mass; }

    public void setMass(double m) { mass = m; }

    public void setGravity(Vector grav) { gravity = grav; }


    public void update() {

        if(isGrounded) {
            jumpsRemaining = jumpCounter;
            velocity = new Vector(velocity.x, 0);
            gravity = new Vector(0, 0);

            if(jumpPressed) {
                gravity = new Vector(0, -.32);
                isGrounded = false;
            }
        }
        else{
            gravity = new Vector(0, -.34);
            velocity.add(gravity);
        }

        //set jump state booleans
        jumpPressed = Keyboard.isKeyDown(Keyboard.KEY_UP) || Keyboard.isKeyDown(Keyboard.KEY_W);


        //move left
        if(Keyboard.isKeyDown(Keyboard.KEY_LEFT) || Keyboard.isKeyDown(Keyboard.KEY_A)) {
            velocity = new Vector(Math.min(-3, velocity.x + 1), velocity.y);
        }

        //move right
        if(Keyboard.isKeyDown(Keyboard.KEY_RIGHT) || Keyboard.isKeyDown(Keyboard.KEY_D)) {
            velocity = new Vector(Math.max(3, velocity.x - 1), velocity.y);
        }

        //friction left
        if(!Keyboard.isKeyDown(Keyboard.KEY_LEFT) && velocity.x < 0) {
            velocity = new Vector(velocity.x * 0.9, velocity.y);
        }

        //friction right
        if(!Keyboard.isKeyDown(Keyboard.KEY_RIGHT) && velocity.x > 0) {
            velocity = new Vector(velocity.x * 0.9, velocity.y);
        }

        //jump or double jump
        if(jumpPressed && jumpsRemaining > 0) {
            velocity.y = jumpVelocity;
            jumpsRemaining--;
        }

        velocity.add(acceleration);
        position.add(velocity);

    }

    public void draw(){
        glPushMatrix();

//        glTranslated(position.x, position.y, 0);

        glBegin(GL_QUADS);

        glColor3d(1, 0, 0);
//        glVertex2d(-8,0);
        glVertex2d(position.x, position.y);

        glColor3d(0, 1, 0);
//        glVertex2d(8,0);
        glVertex2d(position.x + boundBox.x, position.y);

        glColor3d(0, 0, 1);
//        glVertex2d(8,16);
        glVertex2d(position.x + boundBox.x, position.y + boundBox.y);

        glColor3d(1, 1, 0);
//        glVertex2d(-8,16);
        glVertex2d(position.x, position.y + boundBox.y);

        glEnd();

        glPopMatrix();
    }

}