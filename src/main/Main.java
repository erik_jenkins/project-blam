/**
 * Created by Jim on 1/25/15.
 */

package main;

import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;

import bodies.*;
import math.*;
import org.lwjgl.opengl.GL11;
import org.newdawn.slick.Color;
import org.newdawn.slick.util.ResourceLoader;
import physics.Collision;
import physics.CollisionDetector;

import java.awt.Font;
import java.io.InputStream;
import java.util.ArrayList;

import org.newdawn.slick.*;
import physics.PhysicsHandler;

import static org.lwjgl.Sys.getTime;
import static org.lwjgl.opengl.GL11.*;


public class Main {

	public static CollisionDetector collisionDetector;
	public static ArrayList<GameObject> gameObjectList;

	private static TrueTypeFont font;

	private static long lastFPS = getTime();
	private static int fps = 0;

	public static void main(String[] args) throws Exception{

		// set up objects that appear on spawn

		gameObjectList = new ArrayList<GameObject>();
		ArrayList<Collision> noCollisions = new ArrayList<Collision>();

		Player player = new Player(new Vector(300.0, 300.0), new Vector(32.0, 32.0));
		Wall ground = new Wall(new Vector(0.0, 0.0), new Vector(400.0, 32.0));

		Wall leftWall = new Wall(new Vector(0.0, 0.0), new Vector(-50.0, 600.0));
		Wall rightWall = new Wall(new Vector(800.0, 0.0), new Vector(50.0, 600.0));
		Wall topWall = new Wall(new Vector(0, 600), new Vector(600, 50));

		Wall ground2 = new Wall(new Vector(150, 100), new Vector(100,10));


		gameObjectList.add(ground);
		gameObjectList.add(ground2);
		gameObjectList.add(player);
//		gameObjectList.add(leftWall);
//		gameObjectList.add(rightWall);


		CollisionDetector collisionDetector = new CollisionDetector(gameObjectList);

//		ArrayList<Collision> collisionList = collisionDetector.checkCollisions();

		PhysicsHandler physicsHandler = new PhysicsHandler(gameObjectList);



		// create the display window
		Display.setDisplayMode(new DisplayMode(800, 600));
		Display.create();

		// game loop
		while(!Display.isCloseRequested() && !Keyboard.isKeyDown(Keyboard.KEY_Q)){
			setCamera();
			drawBackground();
			physicsHandler.update(collisionDetector.checkCollisions());
			Display.update();
			Display.sync(60);
			updateFPS();
		}
		Display.destroy();

	}




	public static void updateFPS() {
		if (getTime() - lastFPS > 1000) {
			Display.setTitle("Proect Blam     FPS: " + fps);
			fps = 0;
			lastFPS += 1000;
		}
		fps++;
	}


	public static void init() {
		// load a default java font
		Font awtFont = new Font("Times New Roman", Font.BOLD, 16);
		font = new TrueTypeFont(awtFont, true);

		// load font from file
		try {
			InputStream inputStream = ResourceLoader.getResourceAsStream("libs/fonts/data-latin.ttf");
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}


	public static void renderFont() {
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
//		font.drawString(100, 100, Integer.toString(fps), Color.black);
		font.drawString(100, 100, "test", Color.yellow);
	}


	public static void setCamera() {
		//clear the screen
		glClear(GL_COLOR_BUFFER_BIT);
		//modify projection matrix
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		glOrtho(0, 800, 0, 600, -1, 1);

		//modify modelview matrix
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
	}


	public static void drawBackground() {

//		glDisable(GL_TEXTURE_2D);

		//draw the sky
		glBegin(GL_QUADS);

		glColor3d(0.7, 0.8, 0.9);
		glVertex2d(0, 0);
		glVertex2d(800,0);
		glColor3d(0.5, 0.6, 0.8);
		glVertex2d(800,600);
		glVertex2d(0,600);

		glEnd();
	}

}
