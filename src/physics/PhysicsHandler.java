/**
 * Created by Jim on 1/26/15.
 */

package physics;


import bodies.*;
import math.Vector;

import java.util.ArrayList;
import java.util.Iterator;



public class PhysicsHandler {


    private Iterator<Collision> collisionIterator;
    private ArrayList<GameObject> objectList;

    private enum CollisionType {ELASTIC, INELASTIC, PARTIALLY_ELASTIC};


    public PhysicsHandler(ArrayList<GameObject> objects) {
        objectList = objects;
    }



    public void update(ArrayList<Collision> collisionList) {

        collisionIterator = collisionList.iterator();

        while(collisionIterator.hasNext()) {

            Collision collision = collisionIterator.next();

            if(checkCollisionType(collision) == CollisionType.INELASTIC){

                if(collision.getMinorObject().getClass().isInstance(new Wall())) {
                    Player player = (Player) collision.getMajorObject();
                    player.isGrounded = true;
                    collision.resolveCollision();
                }

                else if(collision.getMajorObject().getClass().isInstance(new Wall())) {
                    Player player = (Player) collision.getMinorObject();
                    player.isGrounded = true;
                    collision.resolveCollision();
                }

            }

            else if(checkCollisionType(collision) == CollisionType.ELASTIC){

                System.out.println("ELASTIC COLLISION");

            }
        }

        updateGameObjects();
        renderGameObjects();

    }


    private CollisionType checkCollisionType(Collision collision) {

        if(collision.getMajorObject().getClass().isInstance(new Wall()) || collision.getMinorObject().getClass().isInstance(new Wall())){
            return CollisionType.INELASTIC;
        }

        else{
            return CollisionType.ELASTIC;
        }

    }


    private void updateGameObjects() {

        for(int objectCount = 0; objectCount < objectList.size(); objectCount++){
            objectList.get(objectCount).update();
        }

    }

    private void renderGameObjects() {

        for(int objectCount = 0; objectCount < objectList.size(); objectCount++){
            objectList.get(objectCount).draw();
        }

    }







}
