/**
 * Created by Jim on 1/25/15.
 */

package physics;

import bodies.*;
import math.Vector;

public class Collision {

    private GameObject majorObject, minorObject;
    public enum Direction {X, Y};
    private Direction direction;

    // if Actors are passed, they are isGrounded
    public Collision(GameObject majorObj, GameObject minorObj, Direction dir) {
        majorObject = majorObj;
        minorObject = minorObj;
        direction = dir;
    }



    public GameObject getMajorObject() {
        return majorObject;
    }

    public GameObject getMinorObject() {
        return minorObject;
    }

    public Direction getDirection() { return direction; }


    public void resolveCollision () {

        if(majorObject.getClass().isInstance(new Wall())){

            if(direction == Direction.X) {

                if(majorObject.position().x < minorObject.position().x) {

                    minorObject.position().x = majorObject.position().x + majorObject.getBoundBox().x;

                }

                else {

                    minorObject.position().x = majorObject.position().x - minorObject.getBoundBox().x;

                }

            }

            else {

                if(majorObject.position().y < minorObject.position().y) {

                    minorObject.position().y = majorObject.position().y + majorObject.getBoundBox().y;

                }

                else {

                    minorObject.position().y = majorObject.position().y - minorObject.getBoundBox().y;

                }
            }
        }

        else{

            if(direction == Direction.X) {

                if(minorObject.position().x < majorObject.position().x) {

                    majorObject.position().x = minorObject.position().x + minorObject.getBoundBox().x;

                }

                else {

                    majorObject.position().x = minorObject.position().x - majorObject.getBoundBox().x;

                }

            }

            else {

                if(minorObject.position().y < majorObject.position().y) {

                    majorObject.position().y = minorObject.position().y + minorObject.getBoundBox().y;

                }

                else {

                    majorObject.position().y = minorObject.position().y - majorObject.getBoundBox().y;

                }
            }
        }
    }

}
