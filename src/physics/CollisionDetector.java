/**
 * Created by Jim on 1/25/15.
 */

package physics;

import java.util.ArrayList;
import bodies.*;
import math.*;
import org.lwjgl.input.Keyboard;


public class CollisionDetector {

    ArrayList<GameObject> objectList;


    public CollisionDetector(ArrayList<GameObject> objList) {
        objectList = objList;
    }


    // systematically checks each actor against each other to see if they are isGrounded
    // returns the list of isGrounded actors
    public ArrayList<Collision> checkCollisions() {

        // instantiate two ArrayLists of collisions that will eventually be returned as a tuple
        ArrayList<Collision> collisionList = new ArrayList<Collision>();


        // iterate through the list of Actors
        for(int majorObject = 0; majorObject <= objectList.size() - 2; majorObject++) {

            // iterate through the list of Actors, not including the major actor
            for(int minorObject = majorObject + 1; minorObject <= objectList.size() - 1; minorObject++) {

                if(checkCollisionX(objectList.get(majorObject), objectList.get(minorObject))) {
                    collisionList.add(new Collision(objectList.get(majorObject), objectList.get(minorObject), Collision.Direction.X));
                }

                if(checkCollisionY(objectList.get(majorObject), objectList.get(minorObject))) {
                    collisionList.add(new Collision(objectList.get(majorObject), objectList.get(minorObject), Collision.Direction.Y));
                }

            }
        }
        return collisionList;
    }


    private boolean checkPairOfActors(int majorObject, int minorObject) {
        // check if the minor actor is in the major object
        boolean checkX1 = objectList.get(majorObject).position().x <= objectList.get(minorObject).position().x;
        boolean checkX2 = objectList.get(majorObject).position().x + objectList.get(majorObject).getBoundBox().x >= objectList.get(minorObject).position().x;
        boolean checkY1 = objectList.get(majorObject).position().y <= objectList.get(minorObject).position().y;;
        boolean checkY2 = objectList.get(majorObject).position().y + objectList.get(majorObject).getBoundBox().y >= objectList.get(minorObject).position().y;

        // check if the major actor is in the minor object
        boolean checkX3 = objectList.get(minorObject).position().x <= objectList.get(majorObject).position().x;
        boolean checkX4 = objectList.get(minorObject).position().x + objectList.get(minorObject).getBoundBox().x >= objectList.get(majorObject).position().x;
        boolean checkY3 = objectList.get(minorObject).position().y <= objectList.get(majorObject).position().y;;
        boolean checkY4 = objectList.get(minorObject).position().y + objectList.get(minorObject).getBoundBox().y >= objectList.get(majorObject).position().y;

        return (checkX1 && checkX2 && checkY1 && checkY2) || (checkX3 && checkX4 && checkY3 && checkY4);
    }



    public boolean checkCollisionX(GameObject majorObject, GameObject minorObject) {
        // FOR THE CURRENT FRAME

        // check if the minor actor is in the major object
        boolean checkX1 = majorObject.position().x <= minorObject.position().x;
        boolean checkX2 = majorObject.position().x + majorObject.getBoundBox().x >= minorObject.position().x;

        // check if the major actor is in the minor object
        boolean checkX3 = minorObject.position().x <= majorObject.position().x;
        boolean checkX4 = minorObject.position().x + minorObject.getBoundBox().x >= majorObject.position().x;

        // check if the objects are in the y-bounds of each other
        boolean checkY1 = majorObject.position().y <= minorObject.position().y;
        boolean checkY2 = majorObject.position().y + majorObject.getBoundBox().y >= minorObject.position().y;
        boolean checkY3 = minorObject.position().y <= majorObject.position().y;
        boolean checkY4 = minorObject.position().y + minorObject.getBoundBox().y >= majorObject.position().y;

        // FOR THE NEXT FRAME

        // check if the minor actor is in the major object
        boolean checkNextX1 = majorObject.position().x + majorObject.velocity().x <= minorObject.position().x + minorObject.velocity().x;
        boolean checkNextX2 = majorObject.position().x + majorObject.velocity().x + majorObject.getBoundBox().x >= minorObject.position().x + minorObject.velocity().x;

        // check if the major actor is in the minor object
        boolean checkNextX3 = minorObject.position().x + minorObject.velocity().x <= majorObject.position().x + majorObject.velocity().x;
        boolean checkNextX4 = minorObject.position().x + minorObject.velocity().x + minorObject.getBoundBox().x >= majorObject.position().x + majorObject.velocity().x;


        return !((checkX1 && checkX2) || (checkX3 && checkX4)) && ((checkNextX1 && checkNextX2) || (checkNextX3 && checkNextX4)) && ((checkY1 && checkY2) || (checkY3 && checkY4));
    }



    public boolean checkCollisionY(GameObject majorObject, GameObject minorObject) {
        // FOR THE CURRENT FRAME

        // check if the minor actor is in the major object
        boolean checkY1 = majorObject.position().y <= minorObject.position().y;;
        boolean checkY2 = majorObject.position().y + majorObject.getBoundBox().y >= minorObject.position().y;

        // check if the major actor is in the minor object
        boolean checkY3 = minorObject.position().y <= majorObject.position().y;;
        boolean checkY4 = minorObject.position().y + minorObject.getBoundBox().y >= majorObject.position().y;

        // check if the objects are in the x-bounds of each other
        boolean checkX1 = majorObject.position().x <= minorObject.position().x;
        boolean checkX2 = majorObject.position().x + majorObject.getBoundBox().x >= minorObject.position().x;
        boolean checkX3 = minorObject.position().x <= majorObject.position().x;
        boolean checkX4 = minorObject.position().x + minorObject.getBoundBox().x >= majorObject.position().x;



        // FOR THE NEXT FRAME

        // check if the minor actor is in the major object
        boolean checkNextY1 = majorObject.position().y + majorObject.velocity().y <= minorObject.position().y + minorObject.velocity().y;
        boolean checkNextY2 = majorObject.position().y + majorObject.velocity().y + majorObject.getBoundBox().y >= minorObject.position().y + minorObject.velocity().y;

        // check if the major actor is in the minor object
        boolean checkNextY3 = minorObject.position().y + minorObject.velocity().y <= majorObject.position().y + majorObject.velocity().y;
        boolean checkNextY4 = minorObject.position().y + minorObject.velocity().y + minorObject.getBoundBox().y >= majorObject.position().y + majorObject.velocity().y;


        return !((checkY1 && checkY2) || (checkY3 && checkY4)) && ((checkNextY1 && checkNextY2) || (checkNextY3 && checkNextY4)) && ((checkX1 && checkX2) || (checkX3 && checkX4));
    }

}
