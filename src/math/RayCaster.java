package math;

import bodies.GameObject;

/**
 * Created by jim on 1/30/15.
 */
public class RayCaster {

    private Ray bld, brd, brr, trr, tru, tlu, tll, bll;


    public RayCaster(GameObject gameObject, double mangitude) {

        Vector bl = new Vector(gameObject.position().x, gameObject.position().y);
        Vector br = new Vector(gameObject.position().x + gameObject.getBoundBox().x, gameObject.position().y);
        Vector tl = new Vector(gameObject.position().x, gameObject.position().y + gameObject.getBoundBox().y);
        Vector tr = new Vector(gameObject.position().x + gameObject.getBoundBox().x, gameObject.position().y + gameObject.getBoundBox().y);

        bld = new Ray(bl, new Vector(bl.x, bl.y - mangitude));
        brd = new Ray(br, new Vector(br.x, br.y - mangitude));

        brr = new Ray(br, new Vector(br.x + mangitude, br.y));
        trr = new Ray(tr, new Vector(tr.x + mangitude, tr.y));

        tru = new Ray(tr, new Vector(tr.x, tr.y + mangitude));
        tlu = new Ray(tl, new Vector(tl.x, tl.y + mangitude));

        tll = new Ray(tl, new Vector(tl.x - mangitude, tl.y));
        bll = new Ray(bl, new Vector(bl.x - mangitude, bl.y));
    }


}
