/**
 * Created by Jim on 1/20/15.
 */

package math;

public final class Vector {

    public double x, y;

    public Vector(double x, double y){
        this.x = x;
        this.y = y;
    }


    public double length() {
        return Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2));
    }

    public Vector mulScalar(double scalar) {
        x *= scalar;
        y *= scalar;
        return this;
    }

    public Vector divScalar(double scalar) {
        x /= scalar;
        y /= scalar;
        return this;
    }

    public Vector add(Vector addVec) {
        this.x += addVec.x;
        this.y += addVec.y;
        return this;
    }

    public Vector subtract(Vector subVec) {
        this.x -= subVec.x;
        this.y -= subVec.y;
        return this;
    }

    public double dot(Vector dotVec) {
        return (this.x * dotVec.x) + (this.y * dotVec.y);
    }

}
