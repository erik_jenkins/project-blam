package math;

/**
 * Created by jim on 1/30/15.
 */
public class Ray {

    public Vector tail, head;

    public Ray(Vector tail, Vector head) {
        this.tail = tail;
        this.head = head;
    }


    public Vector length() {
        return new Vector(head.x - tail.x, head.y - tail.y);
    }

    public double magnitude() {
        return Math.sqrt(Math.pow(head.x - tail.x, 2) + Math.pow(head.y - tail.y, 2));
    }

}
