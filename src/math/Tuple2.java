/**
 * Created by Jim on 1/25/15.
 */

package math;


public final class Tuple2<T, U> {

    public T x;
    public U y;

    public Tuple2(T a, U b) {
        x = a;
        y = b;
    }

    public T _1() {
        return x;
    }

    public U _2() {
        return y;
    }

    public void _1(T new_1) {
        x = new_1;
    }

    public void _2(U new_2) {
        y = new_2;
    }

}
